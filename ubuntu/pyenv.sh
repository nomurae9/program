#!/bin/bash


sudo apt update
sudo apt upgrade -y

sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev

if [ -e ~/.pyenv ]; then
	echo "pyenv installed"
else
    git clone https://github.com/yyuu/pyenv.git ~/.pyenv
fi

if [ -e ~/.pyenv/plugins/pyenv-virtualenv ]; then
	echo "virtualenv installed"
else
	git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
fi

if grep pyenv ~/.profile >/dev/null; then
    echo "pyenv installed"
else
    echo 'export PYENV_ROOT=$HOME/.pyenv ' >> ~/.profile
    echo 'export PATH=$PYENV_ROOT/bin:$PATH ' >> ~/.profile
    echo 'eval "$(pyenv init -)" ' >> ~/.profile
    echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.profile
    echo "Please reboot"
fi