import sys, time

class colors:
	BLUE = '\033[34m'
	GREEN = '\033[32m'
	END = '\033[0m'

def postscript(text):
	sys.stdout.write(">>"+text+" ... ")
	sys.stdout.flush()

def ok():
	sys.stdout.write(colors.GREEN + "OK" + colors.END+"\n")
	sys.stdout.flush()

postscript("system checking")
time.sleep(3)
ok()

from progressbar import ProgressBar

min = 0
max = 100

p = ProgressBar(min, max)

for i in range(100):
	time.sleep(1)
	p.update(i)